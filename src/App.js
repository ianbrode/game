import React, { Component }                           from 'react';
import { connect }                                    from 'react-redux';
import { initGame, loseHealth, resetGame, killBug }   from './actions';
import Start                                          from './Start';
import BadEnding                                      from './BadEnding';
import GoodEnding                                     from './GoodEnding';
import PlayGround                                     from './PlayGround';
import './App.css';

class App extends Component {

  render() {
    const { gameStart, getHit, result, status, health, time, reset, kill, bugs } = this.props;

    let view = null;

    if (status === 'NOT_STARTED') view = <Start gameStart={gameStart} />;

    if (status === 'STARTED') {
      view = <PlayGround
        getHit={getHit}
        health={health}
        time={time}
        kill={kill}
        bugs={bugs}
      />;
    }

    if (status === 'ENDED' && result === 'LOSE') view = <BadEnding reStart={reset} bugs={bugs} />;

    if (status === 'ENDED' && result === 'WIN') view = <GoodEnding reStart={reset} bugs={bugs} />;

    return (
      <div className="App noselect">
        { view }
      </div>
    );
  }
}

export default connect(
  state => ({
    result: state.game.result,
    status: state.game.status,
    health: state.user.health,
    time: state.time,
    bugs: state.bugs
  }),
  dispatch => ({
    gameStart: () => dispatch(initGame()),
    getHit: () => dispatch(loseHealth()),
    reset: () => dispatch(resetGame()),
    kill: () => dispatch(killBug())
  })
)(App);
