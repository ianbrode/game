import React      from 'react';
import Heart  from './assets/play/heart.png';
import DeadHeart from './assets/play/heart-dead.png';

const HealthBar = ({health}) => (
  <div className='InfoHealth' style={{width: '26.6vw'}}>
    <img src={ health >= 1 ? Heart : DeadHeart } alt="Logo" className='HealthPane' />
    <img src={ health >= 2 ? Heart : DeadHeart } alt="Logo" className='HealthPane' />
    <img src={ health >= 3 ? Heart : DeadHeart } alt="Logo" className='HealthPane' />
    <img src={ health >= 4 ? Heart : DeadHeart } alt="Logo" className='HealthPane' />
    <img src={ health >= 5 ? Heart : DeadHeart } alt="Logo" className='HealthPane' />
  </div>
);

export default HealthBar;
