import React, { Component } from 'react';
import Howler from 'howler';
import Time  from './assets/play/time.png';
import Bug  from './assets/play/bug.png';

import HealthBar from './HealthBar';
import { formatTime, getRandomInt } from './utils';
import gameSound from './sound/Medvedev_GP.wav';
import throwSound1 from './sound/hit1.wav';
import throwSound2 from './sound/hit2.wav';

import './PlayGround.css';

const girlWidth = 13;
const basicWidth = 100;
const leftBorder = basicWidth / 2; // - (girlWidth / 2);
const rightBorder = leftBorder;

const guyWidth = 16;

const girlStyle = {
  width: `4.7vw`,
  position: 'absolute',
  left: `47.7vw`,
  bottom: '43.6vw',
  height: '2vw',
};

const leftGuyStyle = {
  width: `${guyWidth}vw`,
  cursor: 'pointer',
  position: 'absolute',
  left: `#{0 - guyWidth}vw`,
  bottom: '2.2vw',
  height: '14vw',
  transition: '.1s ease ',
  transitionProperty: 'left bottom'
}

const rightGuyStyle = {
  width: `${guyWidth}vw`,
  cursor: 'pointer',
  position: 'absolute',
  right: '0vw',
  bottom: '2.2vw',
  height: '14vw',
  transition: '.1s ease',
  transitionProperty: 'right bottom'
}

let spawnRate = 98;
let id = 1;
let throwDistance = 10;
const anim = 'throw 1s infinite';

const rapistGenerator = () => {
  let newId = id = id + 1;
  let vertPos = getRandomInt(2, 50);
  return {
    type: getRandomInt(1, 3),
    health: 1,
    dist: 1,
    vertThrow: vertPos,
    vertDir: true,
    vertInit: vertPos,
    damageDealer: false,
    animation: '',
    id: newId
  }
}

const throw1 = new Howler.Howl({
  src: [throwSound1]
});

const throw2 = new Howler.Howl({
  src: [throwSound2]
});

class PlayGround extends Component {
  state = {
    level: 0.5,
    left: [
      rapistGenerator()
    ],
    right: []
  }

  componentDidMount() {

    this.melody = new Howler.Howl({
      src: [gameSound],
      autoplay: true,
      format: 'wav',
      buffer: false,
      loop: true,
      volume: 0.3
    });

    spawnRate = 95;

    this.timer = setInterval(() => {
      let filteredLeft = this.state.left.filter(e => !e.remove);
      let mappedLeft = filteredLeft.map((e) => {
        let dead = e.health <= 0;
        let { vertDir, vertInit, vertThrow, animation } = e;

        let distance;
        if (dead) {
          distance = e.dist - throwDistance;
          vertThrow = vertThrow + (2 + getRandomInt(1, 20));
          animation = anim;
        } else {
          distance = e.dist + this.state.level;
          if (vertThrow > (vertInit + 5) || vertThrow < (vertInit - 5)) vertDir = !vertDir;
          vertThrow = vertDir ? vertThrow + 0.5 : vertThrow - 0.5;
        }

        let result = Object.assign({}, e, { dist: distance, vertThrow, vertDir, animation })
        if (result.thrown || result.damageDealer) result.remove = true;
        if (dead) result.thrown = true;

        if (result.dist >= (leftBorder - guyWidth) && !result.damageDealer){
          this.props.getHit();
          result.damageDealer = true;
        }
        return result;
      });
      if (getRandomInt(1, 100) >= spawnRate) mappedLeft.push(rapistGenerator());

      let filteredRight = this.state.right.filter(e => !e.damageDealer && !e.remove);
      let mappedRight = filteredRight.map((e) => {
        let dead = e.health <= 0;
        let { vertDir, vertInit, vertThrow, animation } = e;

        let distance;
        if (dead) {
          distance = e.dist - throwDistance;
          vertThrow = vertThrow + (2 + getRandomInt(1, 20));
          animation = anim;
        } else {
          distance = e.dist + this.state.level;
          if (vertThrow > (vertInit + 5) || vertThrow < (vertInit - 5)) vertDir = !vertDir;
          vertThrow = vertDir ? vertThrow + 0.5 : vertThrow - 0.5;
        }

        let result = Object.assign({}, e, { dist: distance, vertThrow, vertDir, animation })
        if (result.thrown) result.remove = true;
        if (dead) result.thrown = true;
        if (result.dist >= (rightBorder - guyWidth)){

          this.props.getHit();
          result.damageDealer = true;
        }
        return result;
      });
      if (getRandomInt(1, 100) >= spawnRate) mappedRight.push(rapistGenerator())

      this.setState({
        left: mappedLeft,
        right: mappedRight
      });
    }, 100);
  }

  componentWillUnmount(){
    clearInterval(this.timer);
    this.melody.stop();
    this.melody = null;
  }

  componentWillMount(){
    this.clickRapist = this.clickRapist.bind(this);
  }

  clickRapist(e) {
    let result = e.health = e.health - 1;
    if (result <= 0) {
      this.setState({'level': this.state.level + 0.01});
      spawnRate = spawnRate < 93 ? spawnRate : spawnRate - 0.3;
    }
    return result;
  }

  render() {
    const {health, time, bugs} = this.props;
    return (
      <div className='Play' ref='MainStage'>
        <div className='InfoBoard'>
          <HealthBar health={health}/>
          <div className='InfoHealth BugContainer'>
            <img src={Bug} className='BugIcon'/>
            <span className='BugCounter'>{bugs}</span>
          </div>
          <div className='InfoTimer'>
            <img src={Time} className='InfoTimerIcon'/>
            <span>{ `${formatTime(time)}` }</span>
          </div>
        </div>
        <div className='FigureRowWrapper'>
          <div className='FigureRow'>
            {
              this.state.left.map((e, i) => (
                <div
                  className={`Guy${e.type}-L`}
                  onMouseDown={() => {
                    this.clickRapist(e);
                    i % 2 ? throw1.play() : throw2.play();
                    this.props.kill();
                  }}
                  style={Object.assign({}, leftGuyStyle, {left: `${e.dist}vw`, bottom: `${e.vertThrow}vw`, animation: e.animation})}
                  key={e.id}
                ></div>
              ))
            }
            <div style={girlStyle} className='Girl' />
            {
              this.state.right.map((e, i) => (
                <div
                  className={`Guy${e.type}-R`}
                  onMouseDown={() => {
                    this.clickRapist(e)
                    i % 2 ? throw1.play() : throw2.play();
                    this.props.kill();
                  }}
                  style={Object.assign({}, rightGuyStyle, {right: `${e.dist}vw`, bottom: `${e.vertThrow}vw`, animation: e.animation})}
                  key={e.id}
                ></div>
              ))
            }
          </div>
        </div>
      </div>
    )
  }
};

export default PlayGround;
