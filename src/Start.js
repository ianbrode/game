import React from 'react';
import glass from './assets/main/play_button.png';

const style = {
  position: 'absolute',
  bottom: '1vw',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  cursor: 'pointer',
  width: '30vw',
  height: '10vw'
}

const Start = ({gameStart}) => (
  <div className='Start'>
    <img src={glass} onClick={gameStart} style={style} alt="Logo"/>
  </div>
);

export default Start;
