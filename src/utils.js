const str_pad_left = (string,pad,length) => (new Array(length+1).join(pad)+string).slice(-length);

export const formatTime = (timeInSeconds) => {
  var minutes = Math.floor(timeInSeconds / 60);
  var seconds = timeInSeconds - minutes * 60;
  return str_pad_left(minutes,'0',2)+':'+str_pad_left(seconds,'0',2);
}

export const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
