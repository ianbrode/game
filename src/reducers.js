import {
  START_GAME, END_GAME,
  SET_HEALTH,
  INC_TIMER,
  RESET,
  INC_BUG
} from './actions';
import { combineReducers } from 'redux'

const gameDefaultState = {
  result: null,
  status: 'NOT_STARTED'
};

const game = (state = gameDefaultState, action) => {
  switch (action.type) {
    case RESET:
    case START_GAME:
      return Object.assign({}, state, {
        result: null,
        status: 'STARTED'
      });
    case END_GAME:
      return Object.assign({}, state, {
        result: action.result,
        status: 'ENDED'
      });
    default:
      return state;
  }
}

const userDefaultState = {
  health: 5
};

const user = (state = userDefaultState, action) => {
  switch (action.type) {
    case RESET:
      return { health: 5 }
    case SET_HEALTH:
      return { health: action.health }
    default:
      return state;
  }
}

const timerDefaultState = 60;

const time = (state = timerDefaultState, action) => {
  switch (action.type) {
    case RESET:
      return timerDefaultState;
    case INC_TIMER:
      return state - 1;
    default:
      return state;
  }
}

const bugs = (state = 0, action) => {
  switch (action.type) {
    case RESET:
      return 0;
    case INC_BUG:
      return state + 1;
    default:
      return state;
  }
}

export default combineReducers({
  game,
  user,
  time,
  bugs
});
