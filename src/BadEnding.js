import React, { Component } from 'react';
import SocialBlock from './SocialBlock';
import Howler from 'howler';
import './BadEnding.css';
import botton from './assets/fail/play_again.png';
import gameSound from './sound/Medvedev_lose.wav';
import Bug  from './assets/play/bug.png';

class BadEnding extends Component {
  componentDidMount() {
    this.melody = new Howler.Howl({
      src: [gameSound],
      autoplay: true,
      format: 'wav',
      buffer: false,
      loop: true,
      volume: 0.3
    });
  }
  render() {
    const { bugs } = this.props;
    const desk = `Я убил ${bugs} вирусов, но не уберег Дмитрия Анатольевича. Попробуй и ты спасти его!`;
    return (<div className='BadEnd'>
      <div className='BadEndBug'>
        <span className='BadEndCounter'>{this.props.bugs}</span>
        <img src={Bug} alt="Logo" className='BadEndBugIcon'/>
      </div>
      <SocialBlock
        title='Не уберегли!'
        desk={desk}
        img='https://projects.life.ru/savemedvedev/cap_loose.png'
      />
      <img src={botton} className='BadEndButton' alt="Logo" onClick={this.props.reStart}/>
    </div>)
  }

  componentWillUnmount(){
    this.melody.stop();
    this.melody = null;
  }
}

export default BadEnding;
