import React, { Component } from 'react';
import botton from './assets/fail/play_again.png';
import SocialBlock from './SocialBlock';
import Howler from 'howler';
import './GoodEnding.css';
import gameSound from './sound/Medvedev_victory.wav';
import Bug  from './assets/play/bug.png';

class GoodEnding extends Component {
  componentDidMount() {
    this.melody = new Howler.Howl({
      src: [gameSound],
      autoplay: true,
      format: 'wav',
      buffer: false,
      loop: true,
      volume: 0.3
    });
  }
  render() {
    const { bugs } = this.props;
    const desk = `Я убил ${bugs} вирусов и уберег Дмитрия Анатольевича! Теперь ему не страшны никакие болезни!`;
    return (<div className='GoodEnd'>
      <div className='BadEndBug'>
        <span className='BadEndCounter'>{this.props.bugs}</span>
        <img src={Bug} alt="Logo" className='BadEndBugIcon'/>
      </div>
      <SocialBlock
        title='Уберегли все-таки!'
        desk={desk}
        img='https://projects.life.ru/savemedvedev/cap_win.png'
      />
      <img src={botton} className='GoodEndButton' alt="Logo" onClick={this.props.reStart}/>
    </div>)
  }

  componentWillUnmount(){
    this.melody.stop();
    this.melody = null;
  }
}

export default GoodEnding;
